FROM python:3.10.11

WORKDIR /app
COPY ./requirements.txt /app/requirements.txt
RUN pip install --no-cache-dir --upgrade -r /app/requirements.txt

# RUN python3 -m pip install --upgrade https://storage.googleapis.com/tensorflow/linux/cpu/tensorflow_cpu-2.14.0-cp310-cp310-manylinux_2_17_x86_64.manylinux2014_x86_64.whl

COPY . /app

CMD ["uvicorn", "src.main:app", "--host", "127.0.0.1", "--port", "8000", "--log-config", "log.ini"]
