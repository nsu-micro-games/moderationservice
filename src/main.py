from contextlib import asynccontextmanager
from typing import Annotated
from typing import List
from fastapi import FastAPI, File
from pydantic import BaseModel
import os
import tempfile
import logging


ml_models = {}
predictors ={}

logging.basicConfig(level=logging.INFO, filename="log.log", filemode="a",
                    format="%(asctime)s - %(levelname)s - %(message)s")

@asynccontextmanager
async def lifespan(app: FastAPI):
    from nsfw_detector import predict
    ml_models["mobilenet_v2_140_224"] = \
        predict.load_model('models\mobilenet_v2_140_224.h5')
    logging.info("lifespan -> model mobilenet_v2_140_224 loaded")
    predictors['main'] = predict
    yield
    ml_models.clear()
    logging.info("lifespan -> model mobilenet_v2_140_224 cleared")


app = FastAPI(lifespan=lifespan)

@app.get("/")
async def root():
    return {"message": "For futher information check swagger / redoc"}


@app.post("/scoringWindows")
async def get_score(result: str, file: Annotated[bytes, File()]):
        ans = {}
        logging.info(f"got image {result} at scoringWindows")
        with open(file="test.png", mode="wb") as image:
            temp_file_path = image.name
            image.write(file)
            answer_dict = \
                list(predictors['main'].classify(ml_models["mobilenet_v2_140_224"], temp_file_path).values())[0]
            # 0 or 2 is ok, other prohibited
            isOk = int({(max(enumerate(answer_dict.values()), key=lambda x: x[1]))[0]} <= {0, 2})
            ans[result] = isOk
            logging.info(f"analysis of {result} returned {answer_dict}\n - final response {isOk}")
        return ans


@app.post("/scoring")
async def get_score(result: str, file: Annotated[bytes, File()]):
    ans = {}
    logging.info(f"got image {result} at scoring")
    with tempfile.NamedTemporaryFile() as image:
        temp_file_path = image.name
        image.write(file)
        answer_dict =  \
            list(predictors['main'].classify(ml_models["mobilenet_v2_140_224"], temp_file_path).values())[0]
        # 0 or 2 is ok, other prohibited
        isOk = int({(max(enumerate(answer_dict.values()), key=lambda x: x[1]))[0]} <= {0, 2})
        ans[result] = isOk
        logging.info(f"analysis of {result} returned {answer_dict}\n - final response {isOk}")
    return ans
